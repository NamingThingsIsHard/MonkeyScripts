// ==UserScript==
// @name        Group posts to JSON
// @description Allows creating a JSON of the visible group content complete with all posts and comments in a JSON
// @namespace   http://userscripts.org/users/399688
// @include     https://www.facebookcorewwwi.onion/*
// @include     https://www.facebook.com/*
// @version     1.1
// @grant       GM_setClipboard
// @grant       GM_registerMenuCommand
// @grant       GM_notification
// @noframes
// ==/UserScript==

let allTags = {};

function getTags(text) {
    let tags = text.match(/#\w+/g) || [];
    for (let tag of tags) {
        allTags[tag] = true
    }
    return tags
}

function isLinkExternal(href) {
    return href && !href.match(/^https:\/\/[^\/]+facebook[^\/]+/) && !href.includes('…')
}

/**
 * Get a unique list of all links in the element that don't point to facebook
 */
function getExternalLinks($el) {
    // TODO
    return [... new Set(Array.prototype
        .map.call($el.querySelectorAll("a"), ($a) => {
            $a.dispatchEvent(new Event("mouseover", {bubbles: true}));
            return $a
        })
        .map($link => $link.href)
        .filter(isLinkExternal))]
}

function getTimeIn($el) {
    return Number.parseInt($el.querySelector("[data-utime]").getAttribute("data-utime"))
}


function getAllPosts() {
    return document.querySelectorAll('[id^="mall_post"]');
}

function getPostJSON($post) {
    let $text = getPostTextEl($post);
    let text = $text.innerText;
    let $postPreview = getPostPreviewElement($post)
    let externalLinks = $postPreview ? getExternalLinks($postPreview) : getExternalLinks($text);
    return {
        id: $post.id,
        author: getPostProfile($post),
        text: text,
        links: externalLinks,
        tags: getTags(text),
        comments: getCommentsJON($post),
        timestamp: getTimeIn($post)
    }
}

function getPostProfile($post) {
    let $profile = $post.querySelector(`a.profileLink`);
    if (!$profile) {
        $profile = $post.querySelector(`span a[ajaxify*="/groups/member_bio/"]`)
    }
    return $profile.innerText
}

function getPostTextEl($post) {
    return $post.querySelector(".text_exposed_root, .userContent")
}

function getPostPreviewElement($post) {
    return $post.querySelector(".text_exposed_root + div, .userContent + div")
}

function getComments($post) {
    return $post.querySelectorAll(".UFIComment")
}

function getCommentsJON($post) {
    return Array.prototype.map.call(getComments($post), getCommentJSON)
}

function getCommentJSON($comment) {
    let $text = $comment.querySelector(".UFICommentBody");
    let text = $text.innerText;
    return {
        author: $comment.querySelector(".UFICommentActorName").innerText,
        text: text,
        tags: getTags(text),
        links: getExternalLinks($comment),
        timestamp: getTimeIn($comment)
    }
}


function getJSON() {
    let posts = Array.prototype.map.call(getAllPosts(), getPostJSON);
    return {
        tags: Object.keys(allTags),
        posts
    }
}

function downloadJSON(json) {
    let stringified = JSON.stringify(json, null, 2);
    GM_setClipboard(stringified,"text");
    GM_notification({
        text: "Copied to clipboard",
        title: "Group JSON"
    })
}

GM_registerMenuCommand("Download JSON", () => {
    GM_notification({
        text: "Started",
        title: "Group JSON"
    })
    downloadJSON(getJSON());
} )
