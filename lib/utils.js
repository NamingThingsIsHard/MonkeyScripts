function makeMethodCaller(methodName) {
  return (object) => {
    object[methodName]()
  }
}

function makeAttrGetter(attr) {
  return (object) => object[attr]
}

/**
 * Dynamically call "new" for a given class
 *
 * https://stackoverflow.com/a/8843181
 *
 * @param Cls
 * @return {any}
 */
function newCall(Cls) {
    // Can't apply constructor without calling new (throw's exception)
    // Bind it first as a workaround
    return new (Function.prototype.bind.apply(Cls, arguments));
    // or even
    // return new (Cls.bind.apply(Cls, arguments));
    // if you know that Cls.bind has not been overwritten
}

function concatArrays(arrays){
  return arrays.reduce((output, current) => output.concat(current))
}

function promisedTimeout(timeout = 0) {
    return new Promise((accept, reject) => {
        window.setTimeout(() => {
            accept()
        }, timeout)
    })
}

module.exports = {
    makeAttrGetter,
    makeMethodCaller,
    newCall,
    concatArrays,
    promisedTimeout,
}
