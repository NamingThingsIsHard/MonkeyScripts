/**
 * @typedef {Object} PostToHide
 *
 * @property href {String} - the link of the item to be hidden
 * @property thing {HtmlElement} - the element containing the post
 */

const {makeAttrGetter, promisedTimeout} = require("../utils");
const MAX_HIDE_ATTEMPTS = 3;
const HIDE_DELAY = 1000;

/**
 * Abstract class to take care of hiding things on webpages.
 */
class Tegument {

    /**
     * @param maxAttempts {Number} How many times we should try and hide stuff
     * @param delay {Number} Milliseconds checking if something has been hidden
     * @param itemFormatter {Function<HTMLElement>}
     */
    constructor({
                    maxAttempts = MAX_HIDE_ATTEMPTS,
                    delay = HIDE_DELAY,
                    itemFormatter = makeAttrGetter("innerHTML"),
                }) {
        this.maxAttempts = maxAttempts
        this.delay = delay
        this.formatItem = itemFormatter
    }

    /**
     * Hide the things ;)
     *
     * @param thingsToHide {PostToHide[]}
     * @returns {Promise<void>}
     */
    async hideMany(thingsToHide) {

        // "pre-hide" to reduce staggered layout changes and make visual changes come in a large batch
        for (let {thing} of thingsToHide) {
            this._displayNone(thing)
        }
        for (let thingToHide of thingsToHide) {
            const {thing, href} = thingToHide;
            const formatted = this.formatItem(thing);
            try {
                await this.hideOne(thingToHide);
                console.info("Hid thing", thing, ":", href);
            } catch (e) {
                console.error("Couldn't hide uninteresting link", href, thing, formatted);
            }
        }
    }

    /**
     * Removes the thing from display
     * @param $thing
     */
    _displayNone($thing){
        $thing.style.display = "none"
    }

    /**
     *
     * @param thing {PostToHide}
     * @return {Promise<*|undefined>}
     */
    async hideOne(thing) {
        return this._attemptHideOne(thing)
    }

    /**
     * @param href {String}
     * @param thing {HTMLElement}
     * @param attempt {Number}
     * @return {Promise<*|undefined>}
     * @private
     */
    async _attemptHideOne({href, thing}, attempt = 0) {
        const title = this.formatItem(thing)
        if (attempt >= this.maxAttempts) {
            console.debug("Forcefully hiding", title, ":", href)
            this._hide(thing, true)
            throw "Couldn't hide link"
        }
        console.debug(`Attempt ${attempt + 1 } at hiding: `, title, href);
        this._hide(thing)
        console.debug("Setting timeout to check");

        await promisedTimeout(this.delay);
        if (!this._isHidden(thing)) {
            console.info("Try again", title, ":", href);
            return this._attemptHideOne(thing, ++attempt)
        }
    }

    /**
     * @param $thing
     * @abstract
     * @private
     */
    _isHidden($thing) {
        throw "Not implemented"
    }

    /**
     * @param $thing {HTMLElement}
     * @param force {Boolean} Forcefully hide the element
     * @abstract
     * @private
     */
    _hide($thing, force = false) {
        throw "Not implemented"
    }
}

module.exports = Tegument
