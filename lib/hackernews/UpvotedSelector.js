const BaseSelector = require("../tegument/BaseSelector")

/**
 * Selects posts that have previously been upvoted / liked
 */
class UpvotedSelector extends BaseSelector {

    /**
     * @return {Promise<PostToHide[]>}
     */
    async select() {
        return Array.prototype.filter.call(this._things,($thing) => {
            return $thing.querySelector(".votelinks .nosee")
        }).map(($thing) => {
            return {
                href: $thing.querySelector(this._anchorSelector).href,
                thing: $thing
            }
        })
    }
}

module.exports = UpvotedSelector
