const UpvotedSelector = require("../lib/hackernews/UpvotedSelector")
const {formatItem, ANCHOR_SELECTOR, THING_SELECTOR} = require("../lib/hackernews/utils")

describe("UpvotedSelector", function () {
    beforeAll(function () {
        document.body.innerHTML = __html__["hackernews.html"]
    })

    describe("select", function () {
        it("should have one item", async function (done) {
            const selector = new UpvotedSelector(THING_SELECTOR,
                {
                    anchorSelector: ANCHOR_SELECTOR
                }
            )
            const selections = await selector.select()
            expect(selections.length).toBe(1)

            // Make sure it's the thing that was actually upvoted
            const {href, thing} = selections[0]
            expect(href).toBe("https://arstechnica.com/features/2020/05/the-story-of-cheaper-batteries-from-smartphones-to-teslas/")
            expect(formatItem(thing)).toEqual(
                "How battery costs fell six-fold in a decade"
            )

            done()
        })
    });
});
