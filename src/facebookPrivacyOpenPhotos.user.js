// ==UserScript==
// @name         Facebook: open tagged photos
// @namespace    com.namingthingsishard
// @version      0.1
// @description  Opens the photos you have been tagged in
// @author       CheliferousBenefactor
// @match        https://www.facebookcorewwwi.onion/*/allactivity?privacy_source=activity_log*
// @grant        GM_openInTab
// ==/UserScript==

let timeout = require("../lib/window").timeout;

(async function () {
    'use strict';

    async function doIt() {
        let $photoLinks = document.querySelectorAll('a.profileLink[href*="/photo"]');
        for (let i = 0; i < $photoLinks.length; i++) {
            let $photoLink = $photoLinks[i];
            GM_openInTab($photoLink.href);
            console.log("waiting for next tab");
            await timeout(2500);
        }
    }

    var div = document.createElement('button');
    div.id = 'opener';
    div.innerHTML = 'Open photos';
    div.style = 'z-index: 9999; position: fixed; height: 45px; width: 110px; right: 0px; top: 0px; text-align: center;';

    div.addEventListener("click", doIt);

    var navBar = document.querySelector("#bluebarRoot div[role='navigation']");
    // console.log(navBar);
    navBar.appendChild(div);
    window.SCROLL = true;
    setInterval(() => {
        if (window.SCROLL) {
            window.scrollTo(0, document.querySelector("body").clientHeight);
        }
    }, 1000)
})();
