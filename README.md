A collection of scripts to be used with [GreaseMonkey](http://wiki.greasespot.net/Main_Page) on firefox.

# Requirements

## NodeJS and NPM

**Tested with v.6.8.1**

Follow their [installation guide](https://nodejs.org/en/download/package-manager/) for having a system-wide nodejs version.

You can also use [Node Version Manager](https://github.com/creationix/nvm), which will allow you to switch between node versions easily.

# Build

```bash
# Install dependencies
npm install
# Build a version
grunt
```

The generated files are put into the `dist/` folder. These should be the files that can be redistributed and used by GreaseMonkey.

# Folders

## src/

Only the top-level **.user.js** scripts will be considered to be built and distributed by the `grunt`

## lib/

Where all the common and shared functions should go.

## dist/

All built monkey scripts will be put in here.


# Tech used

 * [NodeJS](http://nodejs.org/) - The server side javascript environment and interpreter
 * [GruntJS](http://gruntjs.com/) - The Javascript task runner
 * [Browserify](http://browserify.org/) - A bundler that creates a single file out of a parent that `require`s all the children.