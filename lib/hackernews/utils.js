module.exports = {
    THING_SELECTOR: "tr.athing",
    ANCHOR_SELECTOR: ".title .storylink",
    formatItem: function ($item) {
        return $item.querySelector(".title a.storylink").innerHTML
    }
}
