const UninterestingSelector = require("../tegument/UninterestingSelector");
const DuplicateSelector = require("../tegument/DuplicateSelector");
const {concatArrays, newCall} = require("../utils");

class HidePostsScript {
    /**
     * @param thingSelector {String}
     * @param tegument {Tegument}
     * @param itemFormatter {Function<HTMLElement>}
     * @param anchorSelector {String}
     */
    constructor(thingSelector, tegument, itemFormatter, anchorSelector) {
        this.thingSelector = thingSelector;
        this.tegument = tegument;
        this.itemFormatter = itemFormatter;
        this.anchorSelector = anchorSelector;
    }

    /**
     *
     * @param extraSelectors {Class<BaseSelector>}
     * @return {Promise<PostToHide>}
     */
    async execute(...extraSelectors) {
        const constructorOptions = {
            itemFormatter: this.itemFormatter,
            anchorSelector: this.anchorSelector
        }
        const selectPromises = [
            // Collect class
            UninterestingSelector,
            DuplicateSelector,
            ...extraSelectors
        ].map((SelectorClass) => {
            // Call constructors
            return newCall(
                SelectorClass,
                this.thingSelector,
                constructorOptions
            )
        }).map(selector => selector.select())
        const results = await Promise.all(selectPromises)
        const toHide = concatArrays(results)
        return this.tegument.hideMany(toHide)
    }
}

module.exports = HidePostsScript
