const {isMatch, AcceptRule, RejectRule, MatchType} = require('../lib/urlMatcher');

describe('isMatch', function () {
  let url = "https://reddit.com/u/Whatever/m/something"
  it('should fallback', () => {
    expect(isMatch(url, [])).toBeFalse()
  });

  describe('preference', function () {
    const rules = [
      new RejectRule(/.*/),
      new AcceptRule("https://reddit.com/.+")
    ];

    it('should prefer accept', () => {
      expect(isMatch(url, rules)).toBeTrue()
    });

    it('should prefer reject', () => {
      const match = isMatch(url, rules, MatchType.REJECT, MatchType.REJECT);
      expect(match).toBeFalse()
    });
  });
});
