const Tegument = require("../tegument/Tegument")
const {getTitle} = require("./thingyverse");

class RedditTegument extends Tegument {
    constructor() {
        super({
            itemFormatter: getTitle
        })
    }

    _hide($thing, force = false) {
        var hideAnchor = $thing.querySelector("form.hide-button a");
        // Reddit has their own function for hiding stuff on old.reddit.com
        // Might do the animation
        window.change_state && window.change_state(hideAnchor, 'hide', window.hide_thing);
        if (hideAnchor) {
            hideAnchor.click();
        }
    }

}

module.exports = RedditTegument
