// ==UserScript==
// @name        Soundcloud scroll
// @namespace   http://userscripts.org/users/399688
// @description Scroll to the end of the likes, because soundclound doesn't allow random sort of the whole collection
// @include     https://soundcloud.com/*
// @version     2
// @grant       none
// ==/UserScript==

/**
 Script for soundclound that still hasn't implemented randomized playing of favorite tracks.

 This will just continuously scroll to the end of the page when the button is hit and stop when hit again.

 Yeah, the CSS sucks giant, monsterballs and it works like shit. So fucking what. It kinda, sorta works.

 Made in Firefox's Ardoise.
 **/

let win = require("../lib/window");
let array = require("../lib/array");

var body = document.getElementsByTagName('body') [0];

// Was a div before. It's now a button to trigger the functionality
var $scrollButton = document.createElement('button');
var interval = null;
$scrollButton.id = 'autoscroller';
$scrollButton.innerHTML = 'Start scrolling';
$scrollButton.style = 'z-index: 9999; position: fixed; height: 45px; width: 110px; right: 0px; top: 0px; text-align: center;';

var $randomButton = document.createElement('button');
$randomButton.id = 'randomAdder';
$randomButton.innerHTML = 'Add random tracks';
$randomButton.style = `
z-index: 9999; 
position: fixed; 
height: 40px; 
width: 125px; 
right: 0px; 
bottom: 5px; 
text-align: center;`;


function activateDiv() {
    interval = setInterval(function () {
        // Will work until they rename the class
        document.getElementsByClassName('l-footer')[0].scrollIntoView()
    }, 500);
    $scrollButton.innerHTML = "Stop scrolling"
}

function deactivateDiv() {
    clearInterval(interval);
    interval = null;
    $scrollButton.innerHTML = "Start scrolling"
}

async function addRandomsToPlaylist() {
    let $menuOpeners = [...document.querySelectorAll(".audibleTile.playableTile .sc-button-more")];
    array.shuffle($menuOpeners);
    // Keep only 100 items
    $menuOpeners = $menuOpeners.slice(0, 100);
    for (let $menuOpener of $menuOpeners) {
        $menuOpener.click();
        let $addNextUp = await win.waitForEl("div.moreActions div.moreActions__group .sc-button-queue.addToNextUp");
        $addNextUp && $addNextUp.click();
    }
}

$scrollButton.addEventListener("click", function (e) {
    interval ? deactivateDiv() : activateDiv();
})
$randomButton.addEventListener("click", addRandomsToPlaylist)

// Wait for the correct URL - add or remove
setInterval(function () {
    var onLikesPage = window.location.href.startsWith("https://soundcloud.com/you/likes")
    if (onLikesPage) {
        if (!document.getElementById($scrollButton.id)) {
            body.appendChild($scrollButton)
        }
        if (!document.getElementById($randomButton.id)) {
            body.appendChild($randomButton)
        }
    } else if (!onLikesPage) {
        deactivateDiv();
        if (document.getElementById($scrollButton.id)) {
            body.removeChild($scrollButton)
        }
        if (document.getElementById($randomButton.id)) {
            body.removeChild($randomButton)
        }
    }
}, 500)
