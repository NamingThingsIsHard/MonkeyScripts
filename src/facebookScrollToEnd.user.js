// ==UserScript==
// @name        Scroll to the end of Recommendations
// @description Sometimes the fucking search of facebook in a group doesn't work. Let's scroll to the end
// @namespace   http://userscripts.org/users/399688
// @include     https://www.facebookcorewwwi.onion/*
// @include     https://www.facebook.com/*
// @version     1.1.0
// @grant       none
// ==/UserScript==

var body = document.getElementsByTagName('body') [0];

// Was a div before. It's now a button to trigger the functionality
var div = document.createElement('button');
var interval = null;
div.id = 'autoscroller';
div.innerHTML = 'Start scrolling';


function clickElement(el) {
    el.click();
}

function clickSelectorAll(selector) {
    document.querySelectorAll(selector).forEach(clickElement);
}

function main() {
    window.scrollTo(0, document.body.scrollHeight);
    // console.log("scrolled");

    // Show comments of closed posts
    clickSelectorAll('a[data-comment-prelude-ref]');

    // View more of each comment
    clickSelectorAll("span.UFICommentBody > span+a[role='button']");

    // See more of each top post / top comment
    clickSelectorAll(".see_more_link_inner");

    // show more comments
    clickSelectorAll(".UFIPagerRow a.UFIPagerLink");

    // console.log("clicked stuff");
}

function activateDiv(){
  interval = setInterval(main, 1000);
  div.innerHTML = "Stop scrolling";
}

function deactivateDiv(){
  clearInterval(interval);
  interval = null;
  div.innerHTML = "Start scrolling";
}

div.addEventListener('click', function(e) {
  interval ? deactivateDiv() : activateDiv();
});

// Wait for the correct URL - add or remove button
setInterval( function() {
    // console.log("interval");
  var onRightPage = window.location.href.includes("/groups/Auditory.Distress.Domain");
  if(onRightPage && !document.getElementById(div.id)) {
      var navBar = document.querySelector("#bluebarRoot div[role='navigation']");
      // console.log(navBar);
      navBar.appendChild(div);
  } else if(!onRightPage && document.getElementById(div.id)){
      deactivateDiv();
      div.remove();
      // console.log("removed");
  }
}, 500);
