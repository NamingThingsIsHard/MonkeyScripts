const {formatItem} = require("./utils");
const Tegument = require("../tegument/Tegument")


class HackerNewsTegument extends Tegument {
    constructor() {
        super({
            itemFormatter: formatItem
        })
    }

    _displayNone($thing) {
        this._hide($thing, true)
    }

    /**
     * Hacker news completely removes the node from the document
     *
     * @param $thing {HTMLElement}
     * @return {boolean}
     */
    _isHidden($thing) {
        return $thing.getRootNode() !== document
    }

    _hide($thing, force = false) {
        const $sibling = $thing.nextElementSibling

        if (force) {
            console.debug("Force hiding")
            $thing.style.display = "none"
            if ($sibling) {
                $sibling.style.display = "none"
            }

        } else if ($sibling) {
            console.debug("Hiding")
            const hideButton = $sibling
                .querySelector(`a[href^="hide?id=${$thing.id}"]`);
            if (hideButton) {
                hideButton.click()
            }
        }
    }
}


module.exports = HackerNewsTegument
