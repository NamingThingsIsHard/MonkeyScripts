// ==UserScript==
// @name        Hide HackerNews posts
// @namespace   com.namingthingsishard
// @description Hides duplicate posts as well as those you keep seeing, but never clicking or voting on
// @include     https://news.ycombinator.com/
// @include     https://news.ycombinator.com/news
// @include     https://news.ycombinator.com/newest
// @version     0.0.1
// @grant       GM.getValue
// @grant       GM.setValue
// ==/UserScript==

const {formatItem, ANCHOR_SELECTOR, THING_SELECTOR} = require("../lib/hackernews/utils");
const UpvotedSelector = require("../lib/hackernews/UpvotedSelector");
const HackerNewsTegument = require("../lib/hackernews/tegument");
const HidePostsScript = require("../lib/scripts/HidePostsScript");

const DEBUG = false;
if (!DEBUG) {
    console.debug = function () {
    };
}

const script = new HidePostsScript(
    THING_SELECTOR,
    new HackerNewsTegument(),
    formatItem,
    ANCHOR_SELECTOR
);

// Only execute the script if a user is logged in
if (document.querySelector("#me")) {
    script.execute(UpvotedSelector)
}
