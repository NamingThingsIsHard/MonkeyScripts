const BaseSelector = require("../lib/tegument/BaseSelector")
const {newCall} = require("../lib/utils")

describe("BaseSelector", function () {

  describe('constructor', function () {
    function defaultParamTest() {
      const selector = newCall(BaseSelector, ...arguments);

      // Default checks
      expect(selector.formatItem).toBeInstanceOf(Function);
      expect(selector._things.length).toEqual(0);
      return selector
    };
    it("should construct with empty object for options", function () {
      defaultParamTest(".selector", {});
    });

    it("should construct with undefined options", function () {
      defaultParamTest(".selector");
    });

    it("should construct with custom itemFormatter", function () {
      const params = {
        itemFormatter: JSON.stringify
      };
      const selector = defaultParamTest(".selector", params);
      expect(selector.formatItem).toBe(params.itemFormatter)
    });

  });

});
