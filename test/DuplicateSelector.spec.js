const DuplicateSelector = require("../lib/tegument/DuplicateSelector")

describe("DuplicateSelector", function () {

    describe("select", function () {
        describe("an empty selection", function () {
            it("should return empty selection", async function (done) {
                const selector = new DuplicateSelector(".selector", {
                    anchorSelector: "a.header"
                });
                expect(await selector.select()).toEqual([])
                done()
            })
        });

        describe("with duplicates", function () {
            it("should get one dupe", async function (done) {
                document.body.innerHTML = __html__["dupes_1.html"]

                const selector = new DuplicateSelector(".item")
                const selection = await selector.select()
                expect(selection.length).toEqual(1)

                const selected = selection[0]
                expect(selected.thing.querySelector("a").innerText)
                    .toEqual("Title3")
                done()
            })

            it("should get multiple dupes", async function (done) {
                document.body.innerHTML = __html__["dupes_n.html"]

                const selector = new DuplicateSelector(".item", {
                    anchorSelector: "a.header"
                })
                const selection = await selector.select()
                expect(selection.length).toEqual(3)

                for await (const selected of selection) {
                    expect(
                        selected.thing.querySelector("a")
                            .innerText.endsWith(" dupe")
                    ).toBeTrue()
                }
                done()
            })
        })

    });
});
