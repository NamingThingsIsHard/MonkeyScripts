// ==UserScript==
// @name        Hide reddit posts
// @namespace   com.namingthingsishard
// @description Hides duplicate posts as well as those you keep seeing, but never clicking or voting on
// @include     https://reddit.com/*
// @include     https://*.reddit.com/*
// @version     1.0.0a
// @grant       GM.getValue
// @grant       GM.setValue
// ==/UserScript==

const {getTitle} = require("../lib/reddit/thingyverse");
const RedditTegument = require("../lib/reddit/tegument");
const {AcceptRule, RejectRule, isMatch, MatchType} = require("../lib/urlMatcher");
const HidePostsScript = require("../lib/scripts/HidePostsScript");
const DEBUG = false;
if (!DEBUG) {
    console.debug = function () {};
}

const script = new HidePostsScript(
    ".linklisting .thing",
    new RedditTegument(),
    getTitle
);

if (isMatch(location.toString(), [
    new RejectRule(/https:\/\/(.+\.)?reddit.com\/user\/.*/),
    new RejectRule(/https:\/\/(.+\.)?reddit.com\/search.*/),
    new RejectRule(/https:\/\/(.+\.)?reddit.com\/submit.*/),
    new RejectRule(/https:\/\/(.+\.)?reddit.com\/submit.*/),
    new RejectRule(/https:\/\/(.+\.)?reddit.com\/.*\/comments\/.*/),
    new RejectRule(/https:\/\/(.+\.)?reddit.com\/r\/.*\/duplicates\/.*/),
    new AcceptRule(/https:\/\/(.+\.)?reddit.com\/user\/.*\/m\/.*/),
], MatchType.ACCEPT)) {
    script.execute()
}
