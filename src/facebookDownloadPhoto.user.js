// ==UserScript==
// @name         Facebook: Auto photo download
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Download the first image visible in the photoset
// @author       DatGoodBway
// @match        https://www.facebookcorewwwi.onion/photo.php?*
// @match        https://www.facebookcorewwwi.onion/*/photos/*
// @match        https://www.facebook.com/photo.php?*
// @match        https://www.facebook.com/*/photos/*
// @grant        GM_download
// ==/UserScript==

let libWindow = require("../lib/window");
let waitForEl = libWindow.waitForEl;

(async function () {
    'use strict';

    let $option = await waitForEl('a[data-action-type="open_options_flyout"]');
    $option.click();

    let $download = await waitForEl('a[data-action-type="download_photo"]');
    let href = $download.href;
    let url = new URL(href);
    let fbid = url.searchParams.get("fbid");

    console.log(`Downloading ${fbid}`)

    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
        GM_download(xhr.responseURL, `${fbid}.jpg`)
    }
    xhr.open("GET", href);
    xhr.send();

})();
