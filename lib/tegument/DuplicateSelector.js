const BaseSelector = require("./BaseSelector")

/**
 * Selects duplicate posts on the page
 */
class DuplicateSelector extends BaseSelector {

    async select(){
        return this._select()
    }

    _select() {
        let seenThings = {};
        let thingsToHide = []
        for (let thing of this._things) {
            let anchor = thing.querySelector(this._anchorSelector);
            let href = anchor.href;

            // Dupe detected!
            if (seenThings[href]) {
                thingsToHide.push({href, thing})
            }
            // Best be sure this shit even exists
            else if (href) {
                seenThings[href] = thing;
            }
        }
        if(thingsToHide.length > 0){
            console.info(`Found ${thingsToHide.length} dupes to hide`)
        } else {
            console.info("No dupes found to hide")
        }
        return thingsToHide
    }
}

module.exports = DuplicateSelector
