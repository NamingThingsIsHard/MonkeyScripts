/**
 * Shit to deal with posts or "things" as they are typed with DOM .classes by reddit
 * @type {{}}
 */
module.exports = {
    getTitle: function (thing) {
        return thing.querySelector(".title a").innerHTML
    },
}