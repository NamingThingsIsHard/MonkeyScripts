// ==UserScript==
// @name        Reddit Expand Everything
// @namespace   com.namingthingsishard
// @description Expands all things it can find
// @include     https://reddit.com/*
// @include     https://www.reddit.com/*
// @exclude     https://reddit.com/*/comments/*
// @exclude     https://*.reddit.com/*/comments/*
// @exclude     https://reddit.com/r/*/duplicates/*
// @exclude     https://*.reddit.com/r/*/duplicates/*
// @version     0.2.1
// ==/UserScript==

const MAX_COUNT = 5;
const INTERVAL = 100;
setTimeout(() => {
    let things = document.querySelectorAll(".linklisting .thing");

    function treatNext(i = 0) {
        if (i >= things.length) {
            console.info("done expanding");
            return
        }
        let thing = things[i];
        const isThingVisible = !thing.classList.contains("hidden") && thing.style.display !== "none";
        let expando = thing.querySelector(".expando-button");
        if (!expando || !isThingVisible) {
            treatNext(++i);
            return
        }
        expando.click();

        // console.log("expando click", el);
        function checkExpando(count = 5) {
            let uninit = thing.querySelector(".expando-uninitialized");
            if (!uninit || count <= 0) {
                // console.log("didn't find uninit", count);
                treatNext(++i);
            } else {
                // console.log("found uninit");
                setTimeout(() => checkExpando(--count), INTERVAL)
            }
        }

        checkExpando()
    }

    treatNext();
}, 500)
