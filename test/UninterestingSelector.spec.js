const UninterestingSelector = require("../lib/tegument/UninterestingSelector")

function mockGreaseMonkeyObject(seen) {
    const db = {seen}
    return {
        getValue: async function (key, value = {}) {
            return db[key] || value
        },
        setValue: async function (key, value) {
            db[key] = value
        }
    }
}

describe("UninterestingSelector", function () {
    beforeAll(function () {
        document.body.innerHTML = __html__["simple.html"]
    })

    describe("select", function () {
        describe("from an empty db", function () {
            beforeEach(function () {
                window.GM = mockGreaseMonkeyObject({})
            })
            afterEach(function () {
                delete window.GM
            })

            it("should return empty selection", async function (done) {
                const selector = new UninterestingSelector(".item", {
                    anchorSelector: "a.title"
                });
                expect(await selector.select()).toEqual([])
                done()
            })
        });

        describe("from a filled db", function () {
            afterEach(function () {
                delete window.GM
            })

            describe("with an item with insufficient views", function () {
                async function testInsufficientViews() {
                    const testUrl = "https://somewhere.test/1";
                    const seen = {
                        [testUrl]: {
                            timesSeen: 1
                        }
                    };
                    window.GM = mockGreaseMonkeyObject(seen)

                    const selector = new UninterestingSelector(".item", {
                        anchorSelector: "a.title"
                    });
                    expect(await selector.select()).toEqual([])
                    return {testUrl, seen};
                }

                it("should not be hidden", async function (done) {
                    await testInsufficientViews()
                    done()
                })

                it("should have other seen items", async function (done) {
                    const {seen} = await testInsufficientViews();

                    const seenLinks = Object.keys(seen);
                    expect(seenLinks.length).toBe(4)

                    // Things should be seen recently
                    for (let seenLink of seenLinks) {
                        const seenThing = seen[seenLink];
                        expect(seenThing.lastSeen).not.toBeNull()
                    }
                    done()
                })

                it("should have been seen again", async function (done) {
                    const {testUrl, seen} = await testInsufficientViews();
                    // TestUrl should have been seen one more time
                    expect(seen[testUrl].timesSeen).toBe(2)
                    done()
                })

            });

            it("should consider one exceeded view", async function (done) {
                const selector = new UninterestingSelector(".item", {
                    anchorSelector: "a.title"
                });
                const seen = {
                    ["https://somewhere.test/1"]: {
                        timesSeen: selector._maxSeenCount
                    }
                };
                window.GM = mockGreaseMonkeyObject(seen)
                const selections = await selector.select()
                expect(selections.length).toEqual(1)
                expect(Object.keys(seen).length).toBe(4)
                done()
            })
            it("should consider all as exceeded", async function (done) {
                const selector = new UninterestingSelector(".item", {
                    anchorSelector: "a.title"
                });

                // Generate db
                const numbers = [
                    1, 2, 3, 4
                ];
                window.GM = mockGreaseMonkeyObject(numbers.reduce((acc, curr) => {
                    acc["https://somewhere.test/" + curr] = selector._maxSeenCount
                    return acc
                }, {}))

                // Test
                const selections = await selector.select();
                expect(selections.length).toEqual(numbers.length)

                done()
            })

        })

    });
});
