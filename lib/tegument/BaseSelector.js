const {makeMethodCaller} = require("../utils");

/**
 * Abstract class to select things that we want to hide using a tegument
 */
class BaseSelector {

    constructor(selector, options={}) {
        const {
            anchorSelector = "a.title",
            itemFormatter = makeMethodCaller("toString")
        } = options;
        // Selects the anchor to use for finding duplicates
        this._anchorSelector = anchorSelector
        this._things = document.querySelectorAll(selector)
        this.formatItem = itemFormatter;
    }

    /**
     * Asynchronously generate a list of things to hide
     * @return {Promise<PostToHide[]>}
     * @abstract
     */
    async select() {
        throw new Error('must be implemented by subclass!');
    }
}

module.exports = BaseSelector
