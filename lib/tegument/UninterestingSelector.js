const BaseSelector = require("./BaseSelector")

// If you come across something this amount of times and you ain't done nothing with it well... tough luck, out it goes!
const MAX_SEEN_COUNT = 5;
const MAX_DAYS_IN_DB = 2;

let now = Date.now();

/**
 * Selects uninteresting posts on the page
 */
class UninterestingSelector extends BaseSelector {

    constructor(selector, options = {}) {
        super(selector, options);
        const {
            maxSeenCount = MAX_SEEN_COUNT,
            maxDaysInDb = MAX_DAYS_IN_DB,
        } = options;
        this._maxSeenCount = maxSeenCount;
        this._maxDaysinDb = maxDaysInDb;
        console.debug("Created UninterestingHideSelector")
    }

    async select() {
        let seen = await GM.getValue("seen", {});
        this._dbHouseKeeping(seen);
        return this._select(seen);
    }

    /**
     *
     * @param seen {Object} basically the db. key=href, value = object
     */
    _select(seen) {
        // All posts
        if (this._things.length <= 0) {
            console.info("Shit's clean yo... nothing to hide!");
            return
        }

        let possiblyUninterestingLinks = [];
        let thingsToHide = [];
        console.debug("Looking for uninteresting posts in ", this._things.length, " items...")

        Array.prototype.forEach.call(this._things, ($thing) => {
            let anchor = $thing.querySelector(this._anchorSelector);
            let href = anchor.href;

            // Don't count duplicates
            if (possiblyUninterestingLinks.includes(href)) {
                return
            }

            let dbThing = seen[href] || {timesSeen: 1};
            console.debug("Got dbThing ", dbThing);
            if (dbThing.timesSeen < this._maxSeenCount) {
                if (dbThing.timesSeen === this._maxSeenCount - 1) {
                    console.info("NEXT on the chopping block!", this.formatItem($thing), href);
                    // Mark with yellow to tell it's next
                    $thing.style.background = "#ffea0080";
                } else {
                    console.debug("Seen", href, "only", dbThing.timesSeen);
                }
                dbThing.timesSeen++;
                dbThing.lastSeen = now;
                seen[href] = dbThing;
                possiblyUninterestingLinks.push(href);
            } else {
                // Mark with red to show we're gonna delete 'em
                $thing.style.background = "#f006";
                thingsToHide.push({href, thing: $thing});
            }
        })

        GM.setValue("seen", seen).then(() => console.info("updated DB"));
        if (thingsToHide.length > 0) {
            console.info(`Uninteresting things to hide: ${thingsToHide.length}`)
        } else {
            console.info("Everything is interesting... huh")
        }
        return thingsToHide
    }

    /**
     * Clean db of old posts
     * @param seen
     * @private
     */
    _dbHouseKeeping(seen) {
        console.debug("cleaning house");
        let cleaned = [];
        for (let key of Object.keys(seen)) {
            let value = seen[key];
            if (value.lastSeen && value.lastSeen < (now - Date.UTC(1970, 0, this._maxDaysinDb))) {
                delete seen[key];
                cleaned.push(key);
            }
        }
        if (cleaned.length > 0) {
            console.debug("Cleaned", cleaned.length, "old links");
        }
    }
}

module.exports = UninterestingSelector
