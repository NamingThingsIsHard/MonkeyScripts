// ==UserScript==
// @name        Subreddit nav shortcuts
// @namespace   com.namingthingsishard
// @description h: move up, j: upvote, k: downvote, l: move down, o: hide, u: click on link, i: go to comments, 0: reload page at top
// @include     https://reddit.com/*
// @include     https://www.reddit.com/*
// @exclude     https://reddit.com/*/comments/*
// @exclude     https://*.reddit.com/*/comments/*
// @exclude     https://reddit.com/r/*/duplicates/*
// @exclude     https://*.reddit.com/r/*/duplicates/*
// @version  1.2.1
// @grant    none
// ==/UserScript==

const ACTIVE_CLASS = "__my_active";

function getThings() {
    return document.querySelectorAll(".linklisting .thing")
}

function getActiveThing() {
    return document.querySelector(`.${ACTIVE_CLASS}`)
}

function isVisible(el) {
    let elTop = el.offsetTop;
    let elBot = elTop + el.offsetHeight;
    let winTop = window.scrollY;
    let winBot = window.scrollY + window.innerHeight;
    let classVisible = !el.classList.contains("hidden") && el.style.display !== "none";

    return classVisible && (elTop < winTop && elBot > winTop) || (elTop > winTop && elTop < winBot)
}

function isInputEl(el){
    return ["TEXTAREA", "INPUT"].includes(el.tagName)
}

function findFirstVisisbleThing() {
    return Array.prototype.find.call(getThings(), isVisible)
}

function setActive(thing, scroll = true) {
    for (let t of getThings()) {
        t.classList.remove(ACTIVE_CLASS)
    }
    thing.classList.add(ACTIVE_CLASS);
    scroll && thing.scrollIntoView();
}

function activate(next = true) {
    let active = getActiveThing();
    if (!active) {
        setActive(findFirstVisisbleThing());
    } else {
        let toActivate = null;
        let things = getThings();
        const activeIndex = Array.prototype.indexOf.call(things, active);

        // Find the next thing to activate
        let i = next ? activeIndex + 1 : activeIndex - 1;
        while (!toActivate && i >= 0 && i < things.length) {
            let item = things[i];
            if (!item.classList.contains("hidden") && item.style.display !== "none") {
                toActivate = item
            } else {
                next ? i++ : i--;
            }
        }

        if (toActivate) {
            setActive(toActivate)
        }

    }
}

/**
 * @callback ActionFunction
 *
 * A function to call when an action is executed
 *
 * @param active {DOMElement}
 * @param accept {Function} to accept the Promise
 * @param whatever {...*} args specific to the function
 */

/**
 * Create a function to be executed when a key is pressed.
 *
 * @param name {String} for logging... sometime in the future
 * @param func {ActionFunction} the "whatever" arguments will be the ones filled when the action is called
 * @returns {function(): Promise<any>}
 */
function createAction(name, func) {
    return function () {
        return new Promise((accept, reject) => {
            let active = getActiveThing();
            if (active) {
                func(active, accept, ...arguments)
            } else {
                accept()
            }
        }).then((next = true) => {
            next && activate();
        });
    }
}

const ACTIONS = {
    vote: createAction("vote", function (active, accept, up = true) {
        let direction = up ? "up" : "down";
        active.querySelector(`.arrow[data-event-action="${direction}vote"]`).click();
        accept();
    }),
    hide: createAction("hide", function (active, accept) {
        active.querySelector(`.hide-button a[data-event-action="hide"]`).click();
        setTimeout(accept, 500);
    }),
    link: createAction("link", function (active, accept) {
        active.querySelector(`a.title`).click();
        accept(false);
    }),
    comment: createAction("comment", function (active, accept) {
        window.open(active.querySelector(`a[data-event-action="comments"]`).href);
        accept(false);
    }),
    reloadTop: function () {
        window.scrollTo(0, 0);
        window.location.reload()
    }
};

let style = document.createElement("style");
style.innerHTML = `
.${ACTIVE_CLASS} {
  border-left-color: darkseagreen;
  border-left-style: dotted;
  border-left-width: 5px;
}
`;

document.querySelector("head").appendChild(style);

document.querySelector("body").addEventListener("keyup", (e) => {
    if(isInputEl(e.target)){

        return
    }
    switch (e.key.toLowerCase()) {
        case "h":
            activate(false);
            break;
        case "l":
            activate();
            break;
        case "j":
            ACTIONS.vote();
            break;
        case "k":
            ACTIONS.vote(false);
            break;
        case "o":
            ACTIONS.hide();
            break;
        case "i":
            ACTIONS.comment();
            break;
        case "u":
            ACTIONS.link();
            break;
        case "0":
            ACTIONS.reloadTop();
            break
    }
})


Array.prototype.forEach.call(getThings(), function (thing) {
    thing.addEventListener("click", (e) => e.target === thing && setActive(e.target, false))
})
