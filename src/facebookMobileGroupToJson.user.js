// ==UserScript==
// @name         Facebook Onion Group to JSON
// @namespace    com.namingthingsishard
// @version      1.1.2
// @description  Backs up a facebook group on the onion site to a JSON
// @author       DatBoi
// @match        https://m.facebookcorewwwi.onion/groups/*
// @grant        GM_listValues
// @grant        GM_deleteValue
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_setClipboard
// @grant        GM_registerMenuCommand
// @grant        GM_notification
// @grant        GM_log
// @grant        GM_openInTab
// @grant        unsafeWindow
// @grant        window.close
// ==/UserScript==

let win = require("../lib/window");

const IS_GROUP = "is group";
const IS_POST = "is post";
const ACTIVE_KEY = "onion group to json active";
const ACTIVE_POST_KEY = "onionGroupActivePost";

let utils = {
    getActiveKeys() {
        return GM_listValues()
            .filter(key => key.startsWith(ACTIVE_POST_KEY));
    },
    start(isActive) {
        if (isActive) {
            GM_notification({
                text: "Started",
                title: "Onion Group to JSON"
            });
            main()
        } else {
            this.getActiveKeys().forEach(GM_deleteValue);
            GM_notification({
                text: "Ended",
                title: "Onion Group to JSON"
            });
        }
    },
    getGroupUrl() {
        let href = window.location.href;
        return href.substr(0, href.lastIndexOf(window.location.pathname) + window.location.pathname.length)
    },
    isLinkExternal(href) {
        try {
            let host = new URL(href);
            return !host.host.includes("facebook")
        } catch (e) {
            return false
        }

    },

    guessPageType() {
        let locationUrl = new URL(window.location.href);
        let id = locationUrl.searchParams.get("id");

        return id ? IS_POST : IS_GROUP;
    },

    getActiveKey(postId) {
        return ACTIVE_POST_KEY + postId
    },
    getGroupId() {
        return [...document.querySelectorAll("a[href*='/groups/']")]
            .map(a => (new URL(a.href)).pathname)
            .map(path => /\/groups\/(\d+)/.exec(path))
            .filter(res => res)
            .map(res => res[1])
            .pop()
    },
    getPostKey(groupId, postId) {
        return `group-${groupId}_post-${postId}`
    },
    getTags(text) {
        return text.match(/#\w+/g) || []
    }

}


GM_registerMenuCommand(
    `Toggle: Onion Group To JSON -> ${!GM_getValue(ACTIVE_KEY, false)}`, () => {
        let isActive = GM_getValue(ACTIVE_KEY, false);
        GM_setValue(ACTIVE_KEY, !isActive);
        isActive = !isActive;
        utils.start(isActive);
    }
);

GM_registerMenuCommand(
    `Copy Onion Group To JSON`, () => {
        let groupId = utils.getGroupId();
        let posts = GM_listValues()
            .filter(key => key.startsWith(`group-${groupId}`))
            .map(GM_getValue)
            .reduce((acc, curr) => {
                acc[curr.id] = curr;
                return acc
            }, {});
        let stringified = JSON.stringify({posts}, null, 2);
        GM_setClipboard(stringified, "text");
        GM_notification({
            text: "Copied to clipboard",
            title: "Onion Group JSON"
        })
    }
);

function main() {
    switch (utils.guessPageType()) {
        case IS_GROUP:
            handleGroup();
            break
        case IS_POST:
            handlePost();
            break
    }
}


async function handleGroup() {

    function openPost(id) {
        GM_log("opening post", id);
        let groupUrl = utils.getGroupUrl();
        GM_openInTab(`${groupUrl}?view=permalink&id=${id}`, true)
    }

    /**
     *
     * @return {Element[]}
     */
    function getPosts() {
        return [...document.querySelectorAll("#m_group_stories_container div[role='article']")]
    }

    let activePosts = utils.getActiveKeys().length;
    for (let $post of getPosts()) {
        // Set post as active
        let dataFt = JSON.parse($post.getAttribute("data-ft"));
        let postId = dataFt["top_level_post_id"];
        let activeKey = utils.getActiveKey(postId);
        if (!postId || GM_getValue(activeKey, false)) {
            continue
        }
        GM_setValue(activeKey, true);

        // Wait before opening tabs
        // The more tabs currently active, the longer the wait
        await win.timeout(activePosts * 1000);

        // Open post
        openPost(postId);

    }

    let $nextPosts = document.querySelector((`a[href^='/groups/${utils.getGroupId()}?bacr=']`));

    if ($nextPosts && GM_getValue(ACTIVE_KEY)) {
        $nextPosts.click();
    } else {
        GM_notification({
            text: "Reached end. Wait for all tabs to close",
            title: "Onion Group to JSON"
        });
    }
}

function handlePost() {
    let $post = document.querySelector("#m_story_permalink_view div[data-ft*='top_level_post_id']");
    let dataFt = JSON.parse($post.getAttribute("data-ft"));

    let postId = dataFt["top_level_post_id"];
    let postKey = utils.getPostKey(utils.getGroupId(), postId);
    let $postText = $post.querySelector("div[data-ft='{\"tn\":\"*s\"}']");
    let postText = ($postText && $postText.innerText) || "";
    let activeKey = utils.getActiveKey(postId);
    GM_log("Active key:", activeKey, GM_getValue(activeKey));

    function getExternalLinks($el) {
        let $as = [...$el.querySelectorAll("a")];
        let links = $as
            .map($a => $a.innerText)
            .concat($as
            // Try to find links that facebook redirects to
                .map($a => $a.href)
                .map((href) => {
                    try {
                        return new URL(href).searchParams.get("u")
                    } catch (e) {
                    }
                }))
        return [...new Set(links.filter(utils.isLinkExternal))]
    }

    let post = GM_getValue(postKey, {
        comments: {}
    });
    post = Object.assign(post, {
        id: postId,
        text: postText,
        links: getExternalLinks($post),
        tags: utils.getTags(postText),
        author: $post.querySelector("table[role='presentation'] h3 strong").innerText,
    })

    function handleComment($comment) {
        let $text = $comment.querySelector("h3 + div");
        let text = $text.innerText;
        let links = getExternalLinks($text);
        let id = $comment.querySelector("span[id^='like_']").id.split("_")[2];
        post.comments[id] = {
            author: $comment.querySelector("h3 a").innerText,
            text: text,
            tags: utils.getTags(text),
            links: links
        };

    }

    // Handle comments
    [...document.querySelectorAll("div[data-ft='{\"tn\":\"R\"}']")]
        .forEach(handleComment);
    GM_log("post val", post);
    GM_setValue(postKey, post);

    let $next = document.querySelector("div[id^='see_prev_'] a");
    if ($next) {
        $next.click();
    } else {
        GM_log("Delete active key", activeKey);
        GM_deleteValue(activeKey);
        GM_log("Active key deleted?", activeKey, GM_getValue(activeKey));
        GM_log("active keys", utils.getActiveKeys());
        window.close();
    }
}

(function () {
    if (!GM_getValue(ACTIVE_KEY, false)) {
        return
    }
    main()
})();
