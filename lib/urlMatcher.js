/**
 *
 * @type {{ACCEPT: number, REJECT: number}}
 */
const MatchType = {
  ACCEPT: 1,
  REJECT: 2,
}

/**
 * @member type {MatchType}
 * @member regex {RegExp}
 */
class MatchRule {
  constructor(type, regex) {
    this.type = type;
    this.regex = regex instanceof RegExp ? regex : new RegExp(regex);
  }

  matches(url){
    return this.regex.test(url)
  }
}

class AcceptRule extends MatchRule {
  constructor(regex) {
    super(MatchType.ACCEPT, regex)
  }
}

class RejectRule extends MatchRule {
  constructor(regex) {
    super(MatchType.REJECT, regex)
  }
}


/**
 *
 * @param url {String}
 * @param rules {MatchRule[]}
 * @param fallback {MatchType}
 * @param precedence {MatchType}
 */
function isMatch(url, rules, fallback=MatchType.REJECT, precedence=MatchType.ACCEPT) {
  let matchResult = null;

  for (let rule of rules) {
    if(!rule.matches(url)){
      continue
    }
    matchResult = rule.type;
    if(precedence === matchResult){
      break
    }
  }
  matchResult = matchResult === null ? fallback : matchResult;
  return matchResult === MatchType.ACCEPT
}

module.exports = {
  isMatch: isMatch,
  MatchRule: MatchRule,
  AcceptRule: AcceptRule,
  RejectRule: RejectRule,
  MatchType: MatchType
}
