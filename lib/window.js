/**
 * Promised window.setTimeout
 *
 * @param func {Function}
 * @param ms {Number | null}
 * @return {Promise<>}
 */
function timeout(func, ms) {
    if (ms === null || ms === undefined) {
        ms = func;
        func = null;
    }
    return new Promise((accept) => {
        setTimeout(() => {
            func && func();
            accept();
        }, ms)
    })
}

async function waitForEl(selector, waitFor=1000) {
    while (!document.querySelector(selector)) {
        await timeout(waitFor)
    }
    return document.querySelector(selector)
}

module.exports = {timeout, waitForEl}
